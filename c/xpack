/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the "Licence").
 * You may not use this file except in compliance with the Licence.
 *
 * You can obtain a copy of the licence at
 * RiscOS/Sources/FileSys/ImageFS/SparkFS/Codecs/SparkSpark/LICENCE.
 * See the Licence for the specific language governing permissions
 * and limitations under the Licence.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the Licence file. If applicable, add the
 * following below this CDDL HEADER, with the fields enclosed by
 * brackets "[]" replaced with your own identifying information:
 * Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 1992 David Pilling.  All rights reserved.
 * Use is subject to license terms.
 */
/*->c.xpack */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "kernel.h"
#include "swis.h"

#include "SparkLib/zflex.h"

#include "Interface/SparkFS.h"

#include "SparkLib/err.h"
#include "SparkLib/sfs.h"

#include "buffer.h"
#include "arcs.h"
#include "unpack.h"
#include "pack.h"


/*****************************************************************************/

#define INITBITS 9
#define CLEAR    256
#define FIRST    257


typedef struct node
{
 unsigned int child;     /* holds (child<<16) + char (8  bits) */
 unsigned int fork;      /* holds (right<<16) + left (16 bits) */
} node;


static node * nd;


static int offset;
static int word;
static int shift;
static int bitsbytes;   /* bits<<3 */
static int bits;        /* number of bits */

static int nextcode;    /* the next free code            */
static int highcode;    /* highest code in this sequence */
static int maxcode;     /* absolute max code             */
static int maxbits;




static buffer * pdest;
static int    * pp;
static int    * px;


#define putcode(code) { \
                       word|=((code)<<shift); \
                       shift+=bits; \
                       if(shift>31) xputcode((code)); \
                      }


static void xputcode(int code)
{
 if(pp<px)
 {
  *pp++=word;
  offset+=32; /* shift; */
  shift-=32;
  word=code>>(bits-shift);
 }
 else
 {
  pdest->p=(char*)pp;
  dumpdestbuffer(pdest);
  pp=(int*)pdest->p;
  px=(int*)pdest->end;
  if(pp<px)
  {
   *pp++=word;
   offset+=32; /* shift; */
   shift-=32;
   word=code>>(bits-shift);
  }
 }
}



static void changebits(void)
{
 int n;

 offset+=shift;
 n=offset % bitsbytes;

/* printf("offset=%d shift=%d n=%d nx=%d bits=%d bitsb=%d\n",
           offset,shift,n,(bitsbytes-n)/bits,bits,bitsbytes); */

 if(n)
 {
  n=(bitsbytes-n)/bits;
  while(n--) putcode(0);
 }

 offset=-shift;
}



static void putc_pak(char c,buffer * dest)
{
 if(dest->p<dest->end) *dest->p++=c;
 else
 {
  dumpdestbuffer(dest);
  if(dest->p<dest->end) *dest->p++=c;
 }
}



static void flushbits(void)
{
 pdest->p=(char*)pp;

 while(shift>0)
 {
  putc_pak(word,pdest);
  word=word>>8;
  shift-=8;
 }
}



static void codeinit(int method)
{
 if(method==CXCOMPRESS || method==CXCOMPRESSZ || method==CXCRUNCH)
 {
  shift=8;
  offset=-shift;
  word=maxbits;
  if(method==CXCOMPRESSZ) word|=128;
 }
 else
 {
  shift=0;
  offset=0;
  word=0;
 }
}



static void tableinit(void)
{
 int i;

 bits=INITBITS;
 bitsbytes=bits<<3;
 highcode=(1<<bits)-1;
 maxcode=(1<<maxbits)-1;

 nextcode=FIRST;

 for(i=0;i<maxcode;i++)
 {
  nd[i].child=0;
  nd[i].fork=0;
 }
}




static _kernel_oserror * packsub(int method,buffer * src,buffer * dest)
{
 _kernel_oserror * err;
 int    c;
 int    scan;
 int    match;
 int    last;
 int    lastc;
 char * p;
 char * x;
 int    notfinished;


 pdest=dest;
 pp=(int*)pdest->p;
 px=(int*)pdest->end;

 err=NULL;
 notfinished=1;

 tableinit();

 codeinit(method);

 if(src->p>=src->end) fillsrcbuffer(src);

 p=src->p;
 x=src->end;

 if(p<x) c=*p++;
 else    notfinished=0;

 while(notfinished)
 {
  /* find match */

  match=c;

  while(1)
  {
   if(p<x)  c=*p++;
   else
   {
    src->p=p;
    fillsrcbuffer(src);
    p=src->p;
    x=src->end;
    if(p<x) c=*p++;
    else    {notfinished=0;break;}
   }

   scan=nd[match].child>>16;
   if(scan>0)
   {
    while(1)
    {
     last=scan;
     lastc=nd[scan].child & 0xFF;
     if(lastc==c) break;
     else
     if(c>lastc)
     {
      scan=nd[scan].fork & 0xFFFF;
      if(scan==0)
      {
       nd[last].fork|=nextcode;
       break;
      }
     }
     else
     {
      scan=nd[scan].fork>>16;
      if(scan==0)
      {
       nd[last].fork|=(nextcode<<16);
       break;
      }
     }
    }

    if(scan>0)
    {
     match=scan;
    }
    else
    {
     break;
    }
   }
   else
   {
    nd[match].child|=nextcode<<16;
    break;
   }
  }

 /* add code */

  putcode(match);

  nd[nextcode].child=c;

  if(nextcode>=maxcode)
  {
   putcode(CLEAR);
   changebits();
   tableinit();
   nextcode--;
  }
  else
  if(nextcode>highcode)
  {
   highcode+=nextcode;
   changebits();
   bits++;
   bitsbytes=bits<<3;
  }

  nextcode++;
 }

/* putcode(CLEAR);
 changebits(); */

 flushbits();

 return(err);
}









static _kernel_oserror * crunchsub(int method,buffer * src,buffer * dest)
{
 _kernel_oserror * err;
 int    c;
 int    scan;
 int    match;
 int    last;
 int    lastc;
 int    notfinished;


 pdest=dest;
 pp=(int*)pdest->p;
 px=(int*)pdest->end;

 err=NULL;
 notfinished=1;

 tableinit();

 codeinit(method);


 c=getcrle(src);
 if(c==EOF) notfinished=0;

 while(notfinished)
 {
  /* find match */

  match=c;

  while(1)
  {
   c=getcrle(src);
   if(c==EOF)
   {
    notfinished=0;
    break;
   }

   scan=nd[match].child>>16;
   if(scan>0)
   {
    while(1)
    {
     last=scan;
     lastc=nd[scan].child & 0xFF;
     if(lastc==c) break;
     else
     if(c>lastc)
     {
      scan=nd[scan].fork & 0xFFFF;
      if(scan==0)
      {
       nd[last].fork|=nextcode;
       break;
      }
     }
     else
     {
      scan=nd[scan].fork>>16;
      if(scan==0)
      {
       nd[last].fork|=(nextcode<<16);
       break;
      }
     }
    }

    if(scan>0)
    {
     match=scan;
    }
    else
    {
     break;
    }
   }
   else
   {
    nd[match].child|=nextcode<<16;
    break;
   }
  }

 /* add code */

  putcode(match);

  nd[nextcode].child=c;

  if(nextcode>=maxcode)
  {
   putcode(CLEAR);
   changebits();
   tableinit();
   nextcode--;
  }
  else
  if(nextcode>highcode)
  {
   highcode+=nextcode;
   changebits();
   bits++;
   bitsbytes=bits<<3;
  }

  nextcode++;
 }

/* putcode(CLEAR);
 changebits(); */

 flushbits();

 return(err);
}







/***************************************************************************/


void closexpack(void)
{
 if(nd) flex_free((flex_ptr)&nd);
}



_kernel_oserror * openxpack(int bits)
{
 _kernel_oserror * err;
 maxbits=bits;
 err=flex_alloc((flex_ptr)&nd,((1<<maxbits)+1)*sizeof(node));
 return(err);
}



_kernel_oserror * xpack(int method,heads * hdr,buffer * src,buffer * dest)
{
 _kernel_oserror * err;

 if(method==CXCRUNCH) err=crunchsub(method,src,dest);
 else                 err=packsub(method,src,dest);


 if(!err)
 {
  clearcode(dest);
  err=dumpdestbuffer(dest);
  hdr->size=dest->left;
 }

 return(err);
}
