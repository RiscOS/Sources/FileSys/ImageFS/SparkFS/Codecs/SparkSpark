/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the "Licence").
 * You may not use this file except in compliance with the Licence.
 *
 * You can obtain a copy of the licence at
 * RiscOS/Sources/FileSys/ImageFS/SparkFS/Codecs/SparkSpark/LICENCE.
 * See the Licence for the specific language governing permissions
 * and limitations under the Licence.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the Licence file. If applicable, add the
 * following below this CDDL HEADER, with the fields enclosed by
 * brackets "[]" replaced with your own identifying information:
 * Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 1992 David Pilling.  All rights reserved.
 * Use is subject to license terms.
 */
/*->h.buffer */

#define DEFPACKSIZE   0x4000
#define DEFUNPACKSIZE 0x4000


#define BCRC    0x1
#define BCODE   0x2


typedef struct buffer
{
 char       * data;     /* pointer to start of buffer        */
 char       * p;        /* pointer into data                 */
 char       * f;        /* another pointer into data         */
 char       * end;      /* pointer to end of data !          */
 int          buffsize; /* length of buffer                  */
 int          datasize; /* amount of data to read            */
 int          left;     /* amount of data left to read       */
 int          fh;       /* file handle to refill buffer from */
 int          flags;    /* control CRC and codec             */
 int          crc;
 flex_lockstr lock;
} buffer;


#define COPYCRC    0x1
#define COPYCODE   0x2
#define COPYDECODE 0x4


extern void initbuff(buffer * b,char * data,int datasize,int buffsize,int fh);
extern void finitbuff(buffer * b);
extern _kernel_oserror * initsrcbuffer(buffer * src);
extern _kernel_oserror * fillsrcbuffer(buffer * src);
extern _kernel_oserror * dumpdestbuffer(buffer * dest);
extern _kernel_oserror * dumpblock(buffer * dest,char * p,int len);
extern _kernel_oserror * copyfile(int source,int dest,int length,int flags);
extern _kernel_oserror * writeblock(char ** source,int dest,int length,
                                                                 int flags);
extern _kernel_oserror * readblock(char ** dest,int source,int length,
                                                                 int flags);
extern _kernel_oserror * getfirstbyte(archive * arc,int fh,char * b,int at);

extern int copylen;
extern int copycrc;

extern void setcode(archive * arc);
extern void closecode(void);
extern void clearcode(buffer * dest);

extern _kernel_oserror * buff_alloc(flex_ptr b,int size,int * bsize);
extern void garbleblock(char * data,int len);

extern _kernel_oserror * reinitsrc(buffer * src);

extern int coderound(archive * arc,int length);


extern _kernel_oserror * bf_read(int fh,void ** b,int offset,int n);
extern _kernel_oserror * bf_write(int fh,void ** b,int offset,int n);
